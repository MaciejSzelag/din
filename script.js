const btnResult = document.querySelector("button.result");
const btnReset = document.querySelector("button.reset");
const inputOne = document.querySelector("input.one");
const inputTwo = document.querySelector("input.two");
const inputFive = document.querySelector("input.five");
const diff = document.querySelector("p.diff");
const sg = document.querySelector("p.sg");
const lwt = document.querySelector("p.lwt");
const last = document.querySelector("p.lostvol");
const values = [];
const subtraction = (a, b) => {
    return a - b;
};
const division = (a, b) => {
    return a / b;
};
const times = (a, b) => {
    return a * b;
};
const llwt = (a, b, c) => {
    return (a - b) / c;
};
btnReset.addEventListener("click", () => {
    values.length = 0;
    inputOne.value = "";
    inputTwo.value = "";
    inputFive.value = "";
    diff.textContent = "";
    sg.textContent = "";
    lwt.textContent = "";
    last.textContent = "";
});

btnResult.addEventListener("click", e => {
    e.preventDefault();
    let first = parseFloat(inputOne.value);
    let second = parseFloat(inputTwo.value);
    let five = parseFloat(inputFive.value);
    const sub = subtraction(first, second).toFixed(3);
    let sgResult = division(first, sub).toFixed(3);
    diff.textContent = sub;
    sg.textContent = division(first, sub).toFixed(3);
    lwt.textContent = subtraction(first, five).toFixed(3);
    last.textContent = (
        llwt(first, five, sgResult).toFixed(4) * 1000
    ).toFixed(1);
    // console.log(`${first} and ${second}  and ${five}`);
    // values.push(first);
    // values.push(second);
    // values.push(five);
    // console.log(values);
});