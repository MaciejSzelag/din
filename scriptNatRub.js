//current date
const currentDate = document.getElementById("date");
currentDate.textContent = new Date().toLocaleDateString();
//sample 1 P0
const sam1p01 = document.querySelector(".s1P0 .p01");
const sam1p02 = document.querySelector(".s1P0 .p02");
const sam1p03 = document.querySelector(".s1P0 .p03");
//sample 1 P30
const sam1p301 = document.querySelector(".s1P30 .p301");
const sam1p302 = document.querySelector(".s1P30 .p302");
const sam1p303 = document.querySelector(".s1P30 .p303");
//sample 2 P0
const sam2p01 = document.querySelector(".s2P0 .p01");
const sam2p02 = document.querySelector(".s2P0 .p02");
const sam2p03 = document.querySelector(".s2P0 .p03");
//sample 2 P30
const sam2p301 = document.querySelector(".s2P30 .p301");
const sam2p302 = document.querySelector(".s2P30 .p302");
const sam2p303 = document.querySelector(".s2P30 .p303");
//sample 3 P0
const sam3p01 = document.querySelector(".s3P0 .p01");
const sam3p02 = document.querySelector(".s3P0 .p02");
const sam3p03 = document.querySelector(".s3P0 .p03");
//sample 3 P30
const sam3p301 = document.querySelector(".s3P30 .p301");
const sam3p302 = document.querySelector(".s3P30 .p302");
const sam3p303 = document.querySelector(".s3P30 .p303");
//sample 4 P0
const sam4p01 = document.querySelector(".s4P0 .p01");
const sam4p02 = document.querySelector(".s4P0 .p02");
const sam4p03 = document.querySelector(".s4P0 .p03");
//sample 4 P30
const sam4p301 = document.querySelector(".s4P30 .p301");
const sam4p302 = document.querySelector(".s4P30 .p302");
const sam4p303 = document.querySelector(".s4P30 .p303");
//sample 5 P0
const sam5p01 = document.querySelector(".s5P0 .p01");
const sam5p02 = document.querySelector(".s5P0 .p02");
const sam5p03 = document.querySelector(".s3P0 .p03");
//sample 5 P30
const sam5p301 = document.querySelector(".s5P30 .p301");
const sam5p302 = document.querySelector(".s5P30 .p302");
const sam5p303 = document.querySelector(".s5P30 .p303");
//results
const res1_1 = document.querySelector(".s1P0 .result");
const res1_2 = document.querySelector(".s1P30 .result");
const res2_1 = document.querySelector(".s2P0 .result");
const res2_2 = document.querySelector(".s2P30 .result");
const res3_1 = document.querySelector(".s3P0 .result");
const res3_2 = document.querySelector(".s3P30 .result");
const res4_1 = document.querySelector(".s4P0 .result");
const res4_2 = document.querySelector(".s4P30 .result");
const res5_1 = document.querySelector(".s5P0 .result");
const res5_2 = document.querySelector(".s5P30 .result");
//buttons
const showResultOne = document.querySelector(".s1P0 .resultp0p30");
const showResultTwo = document.querySelector(".s2P0 .resultp0p30");
const showResultThree = document.querySelector(".s3P0 .resultp0p30");
const showResultFour = document.querySelector(".s4P0 .resultp0p30");
const showResultFive = document.querySelector(".s5P0 .resultp0p30");
//array
let avg_allOf = [];
class CountAvg {
    constructor(s1, s2, s3) {
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
    }
    computeAvg() {
        return ((parseFloat(this.s1) + parseFloat(this.s2) + parseFloat(this.s3)) / 3).toFixed(1);

    }
    computePerCent() {
        return ((parseFloat(this.s1) / parseFloat(this.s2)) * 100).toFixed(1);
    }

}
class Warning {
    constructor(result) {
        this.result = result;
    }
    less() {
        if (this.result <= 49) {
            alert("Result is less than 50%");
        }
    }
}

const sam1 = function () {
    const avgP0 = new CountAvg(sam1p01.value, sam1p02.value, sam1p02.value);
    const avgP30 = new CountAvg(sam1p301.value, sam1p302.value, sam1p303.value);
    const perCent_sam = new CountAvg(avgP30.computeAvg(), avgP0.computeAvg());
    const ups = new Warning(perCent_sam.computePerCent())
    res1_1.textContent = avgP0.computeAvg();
    res1_2.textContent = avgP30.computeAvg();
    this.textContent = perCent_sam.computePerCent() + "%";
    ups.less()
    if (avg_allOf.length >= 1) {
        alert("array is full");
        return;
    }
    if (perCent_sam.computePerCent() <= 49) {
        this.style.color = 'red'
    } else {
        this.style.color = 'black'
    }
    avg_allOf.push(parseFloat(showResultOne.textContent));
    console.log(avg_allOf);
};
showResultOne.addEventListener("click", sam1);
const sam2 = function () {
    const avgP0 = new CountAvg(sam2p01.value, sam2p02.value, sam2p02.value);
    const avgP30 = new CountAvg(sam2p301.value, sam2p302.value, sam2p303.value);
    const perCent_sam = new CountAvg(avgP30.computeAvg(), avgP0.computeAvg());
    res2_1.textContent = avgP0.computeAvg();
    res2_2.textContent = avgP30.computeAvg();
    this.textContent = perCent_sam.computePerCent() + "%";
    if (avg_allOf.length >= 2) {
        alert("array is full");
        return;
    }
    if (perCent_sam.computePerCent() <= 49) {
        this.style.color = 'red'
    } else {
        this.style.color = 'black'
    }
    avg_allOf.push(parseFloat(perCent_sam.computePerCent()));
    console.log(avg_allOf);
};
showResultTwo.addEventListener("click", sam2);
const sam3 = function () {
    const avgP0 = new CountAvg(sam3p01.value, sam3p02.value, sam3p02.value);
    const avgP30 = new CountAvg(sam3p301.value, sam3p302.value, sam3p303.value);
    const perCent_sam = new CountAvg(avgP30.computeAvg(), avgP0.computeAvg());
    res3_1.textContent = avgP0.computeAvg();
    res3_2.textContent = avgP30.computeAvg();
    this.textContent = perCent_sam.computePerCent() + "%";
    if (avg_allOf.length >= 3) {
        alert("array is full");
        return;
    }
    if (perCent_sam.computePerCent() <= 49) {
        this.style.color = 'red'
    } else {
        this.style.color = 'black'
    }
    avg_allOf.push(parseFloat(perCent_sam.computePerCent()));
    console.log(avg_allOf);
};
showResultThree.addEventListener("click", sam3);
const sam4 = function () {
    const avgP0 = new CountAvg(sam4p01.value, sam4p02.value, sam4p02.value);
    const avgP30 = new CountAvg(sam4p301.value, sam4p302.value, sam4p303.value);
    const perCent_sam = new CountAvg(avgP30.computeAvg(), avgP0.computeAvg());
    res4_1.textContent = avgP0.computeAvg();
    res4_2.textContent = avgP30.computeAvg();
    this.textContent = perCent_sam.computePerCent() + "%";
    if (avg_allOf.length >= 4) {
        alert("array is full");
        return;
    }
    if (perCent_sam.computePerCent() <= 49) {
        this.style.color = 'red'
    } else {
        this.style.color = 'black'
    }
    avg_allOf.push(parseFloat(perCent_sam.computePerCent()));
    console.log(avg_allOf);
};
showResultFour.addEventListener("click", sam4);
const sam5 = function () {
    const avgP0 = new CountAvg(sam5p01.value, sam5p02.value, sam5p02.value);
    const avgP30 = new CountAvg(sam5p301.value, sam5p302.value, sam5p303.value);
    const perCent_sam = new CountAvg(avgP30.computeAvg(), avgP0.computeAvg());
    res5_1.textContent = avgP0.computeAvg();
    res5_2.textContent = avgP30.computeAvg();
    this.textContent = perCent_sam.computePerCent() + "%";
    if (avg_allOf.length >= 5) {
        alert("array is full");
        return;
    }
    if (perCent_sam.computePerCent() <= 49) {
        this.style.color = 'red'
    } else {
        this.style.color = 'black'
    }
    avg_allOf.push(parseFloat(perCent_sam.computePerCent()));
    console.log(avg_allOf);
};
showResultFive.addEventListener("click", sam5);
const finalResult = document.querySelector(".s-all-avg .final-result");
finalResult.addEventListener("click", function () {
    let sum = 0;
    for (i = 0; i < avg_allOf.length; i++) {
        sum += avg_allOf[i];
    }
    finalResult.textContent = (sum / avg_allOf.length).toFixed(1);
    console.log(sum);
});
console.log(avg_allOf);