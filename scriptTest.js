const btnResult = document.querySelector("button.result");
const btnReset = document.querySelector("button.reset");
const inputOne = document.querySelector("input.one");
const inputTwo = document.querySelector("input.two");
const inputFive = document.querySelector("input.five");
const diff = document.querySelector("p.diff");
const sg = document.querySelector("p.sg");
const lwt = document.querySelector("p.lwt");
const last = document.querySelector("p.lostvol");
const values = [];
btnReset.addEventListener("click", () => {
    values.length = 0;
    inputOne.value = "";
    inputTwo.value = "";
    inputFive.value = "";
    diff.textContent = "";
    sg.textContent = "";
    lwt.textContent = "";
    last.textContent = "";
});

class Result {
    constructor(a, b, c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    sub() {
        return this.a - this.b;
    }
    div() {
        return this.a / this.b;
    }
    mult() {
        return this.a * this.b
    }
    Result() {
        return ((this.a - this.b) / this.c) * 1000;
    }
}
const result = (e) => {
    e.preventDefault();
    let first = parseFloat(inputOne.value);
    let second = parseFloat(inputTwo.value);
    let five = parseFloat(inputFive.value);
    const one_two = new Result(first, second)
    const one_five = new Result(first, five)
    const SG = new Result(first, one_two.sub())
    const lostvol = new Result(first, five, SG.div())
    diff.textContent = (one_two.sub()).toFixed(3);
    sg.textContent = (SG.div()).toFixed(3)
    lwt.textContent = (one_five.sub()).toFixed(3);
    last.textContent = lostvol.Result().toFixed(1)
}
btnResult.addEventListener("click", result);